import React from 'react';
import { StyleSheet, Text, View, TextInput, Alert, TouchableOpacity } from 'react-native';

class AlertText extends React.Component{

  constructor(props, context)
  {
    super(props, context);
    this.state = {text: ''}
  }

  componentWillMount()
  {
    console.log("COMPONENTE AINDA VAI MONTAR")
  }

  componentDidMount()
  {
    console.log("COMPONENTE FOI MONTADO")
  }

  componentDidUpdate()
  {
    console.log("COMPONENTE ATUALIZOU")
  }

  /*--------*/

  alertarMensagem()
  {
    Alert.alert('Alerta', this.state.text);
  }

  renderButton()
  {
    if(this.state.text.length > 5){
      return (
        <TouchableOpacity style={{borderWidth: 1, padding: 10, backgroundColor: 'orange'}}
          onPress={() => this.alertarMensagem()}>
          <Text style={{padding: 10}}>Exibir Alerta</Text>
        </TouchableOpacity>
      )
    }else{
      return null;
    }
  }

  /*--------*/
  

  render()
  {
    return (
      <View style={{width: '100%'}}>
        <TextInput 
            onChangeText={textoDigitado => this.setState({text: textoDigitado})}
            style={{width: '50%', borderWidth: 1, borderColor: '#000'}}></TextInput>
        {this.renderButton()}
      </View>
    )
  }

}

export default function App() {

  return (
    <View style={styles.container}>
      <Text>Hello World!</Text>
      <View style={{width: '100%', height: 200, marginTop: 20}}>
        <AlertText defaultTitle={'Teste'}></AlertText>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
